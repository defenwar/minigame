#include "cocos2d.h"
class Game : public cocos2d::Scene
{
	public:
	static cocos2d::Scene* createScene();

	virtual bool init();
	bool animation();
	bool shoot();
	// implement the "static create()" method manually
	CREATE_FUNC(Game);
};


