#ifndef __GAME_SCENE_H__
#define __GAME_SCENE_H__
#define _N_ 5
#define _M_ 5

#include "cocos2d.h"

class Game : public cocos2d::Scene
{
public:
	static cocos2d::Scene* createScene();
	
	virtual bool init();

	virtual void firstStart(cocos2d::Ref* pSender);
	
	virtual bool reloaded();//Restart
	virtual void shoot(cocos2d::Vec2 pos, std::string image,bool baf,short unsigned int countRebound, cocos2d::Sprite* prevBlock);//shoot
	virtual bool onTouchBegan(cocos2d::Touch*, cocos2d::Event*);//Tap
	virtual void onClickRestart(cocos2d::Ref* pSender);//button Restart
	virtual void onClickClose(cocos2d::Ref* pSender);//Button Close(exit)
	virtual void UpdateTimer(float dt);//Timer


	// implement the "static create()" method manually
	CREATE_FUNC(Game);
private:
	cocos2d::Label* labelTouch;//"PEW"
	cocos2d::Sprite* blocks[_N_][_M_];//Blocks
	std::string blockType[_N_][_M_];//type Blocks
	float blockTime[_N_][_M_];//time to Regeneration
	cocos2d::Label* score;//label Score
	int currentScore = 0;//current Score
	bool good = true;//gameover
	float timeGame;//time Game
	
	cocos2d::Color3B backGroundColor;
};


#endif // __GAME_SCENE_H__