#include "GameScene.h"
#include <iostream>
#include <string>
#define timeaction 0.5
USING_NS_CC;

Scene* Game::createScene()
{
	return Game::create();
}

bool Game::init()
{
	if (!Scene::init())
	{
		return false;
	}
	auto visibleSize = Director::getInstance()->getVisibleSize();

	auto backGround = Sprite::create("backGround.png");
	backGround->setContentSize(Size(visibleSize.width, visibleSize.height));
	backGround->setAnchorPoint(Vec2(0.0, 0.0));
	this->addChild(backGround);

	auto startButton = MenuItemImage::create(
		"start.png",
		"start2.png",
		CC_CALLBACK_1(Game::firstStart, this));
	startButton->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));

	auto exitButton = MenuItemImage::create(
		"CloseNormal.png",
		"CloseSelected.png",
		CC_CALLBACK_1(Game::onClickClose, this));
	exitButton->setPosition(Vec2(visibleSize.width- 25, visibleSize.height-25));
	
	auto menu = Menu::create(startButton,exitButton, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);	

	return true;
}

void Game::firstStart(cocos2d::Ref* pSender)
{
	this->removeAllChildren();

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->onTouchBegan = CC_CALLBACK_2(Game::onTouchBegan, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

	reloaded();
}
bool Game::reloaded()//create blocks and ease interface
{
	good = true;
	currentScore = 0;

	timeGame = 0;
	this->schedule(schedule_selector(Game::UpdateTimer), 0.001f);

	auto visibleSize = Director::getInstance()->getVisibleSize();

	//background
	auto backGround = Sprite::create("backGround.png");
	backGround->setContentSize(Size(visibleSize.width, visibleSize.height));
	backGround->setAnchorPoint(Vec2(0.0, 0.0));
	this->addChild(backGround);
;
	//save size
	auto OriginalX = visibleSize.width / 2 - 16 * _M_;
	auto x = 0;
	auto OriginalY = visibleSize.height - 100;
	auto y = 0;

	// ranom type Block
	for (int i = 0; i < _N_; i++)
		for (int j = 0; j < _M_; j++)
		{
			blockTime[i][j] = 0;
			int type = rand() % 6;

			switch (type)
			{
			case 0:
				blockType[i][j] = "coubfraction.png";
				break;
			case 1:
				blockType[i][j] = "coubrebound.png";
				break;
			case 2:
				blockType[i][j] = "coubregeneration.png";
				break;
			default:
				blockType[i][j] = "coub.png";
				break;
			}
		}

	//create block
	for (int i = 0; i < _N_; i++)
	{
		for (int j = 0; j < _M_; j++)
		{
			cocos2d::Vector<cocos2d::FiniteTimeAction*> animation;
			animation.pushBack(MoveBy::create(timeaction, Point(x, 0)));
			animation.pushBack(MoveBy::create(timeaction / 3, Point(0, y)));

			blocks[i][j] = Sprite::create(blockType[i][j]);

			blocks[i][j]->setAnchorPoint(Vec2(0.0, 0.0));
			blocks[i][j]->setPosition(Vec2(OriginalX, OriginalY));
			blocks[i][j]->setContentSize(Size(30, 30));

			this->addChild(blocks[i][j]);

			auto sequence = cocos2d::Sequence::create(animation);
			blocks[i][j]->runAction(EaseIn::create(sequence, 0.5));
			x += 32;
		}
		x = 0;
		y -= 32;
	}

	//score label
	score = Label::createWithSystemFont("Score: " + std::to_string(currentScore), "Arial", 24);
	score->setPosition(visibleSize.width / 1.3, 30);
	this->addChild(score);

	//"PEW" label :)
	labelTouch = Label::createWithSystemFont("PEW", "Arial", 24);
	labelTouch->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2));
	labelTouch->setVisible(false);
	this->addChild(labelTouch);

	auto exitButton = MenuItemImage::create(
		"CloseNormal.png",
		"CloseSelected.png",
		CC_CALLBACK_1(Game::onClickClose, this));
	exitButton->setPosition(Vec2(visibleSize.width - 25, visibleSize.height - 25));

	auto menu = Menu::create( exitButton, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	return true;
}

void Game::shoot(cocos2d::Vec2 pos, std::string image, bool baf, short unsigned int countRebound, Sprite* prevBlock)//one shoot animation and incriminate score
{
	//animation "PEW"
	cocos2d::Vec2 position;
	if (baf)
	{
		labelTouch->setColor(cocos2d::Color3B::BLACK);
		labelTouch->setOpacity(255);
		cocos2d::Vector<cocos2d::FiniteTimeAction*> animation;
		animation.pushBack(MoveBy::create(timeaction, Point(-50, 0)));
		animation.pushBack(TintTo::create(timeaction, 255, 0, 0));
		animation.pushBack(FadeTo::create(timeaction, 0));
		auto parallel = cocos2d::Spawn::create(animation);
		labelTouch->runAction(EaseIn::create(parallel, 0.5));
	}
	
	//create bullet
	auto bullet = Sprite::create(image);
	bullet->setAnchorPoint(Vec2(0.0, 0.0));
	bullet->setContentSize(Size(20, 20));
	if (image=="fraction.png") bullet->setContentSize(Size(14, 14));
	bullet->setPosition(pos);
	cocos2d::Vector<cocos2d::FiniteTimeAction*> animationBullet;


	Size size;
	int i=0, j=0;

	{//random until we find blocks[][].size more than zero
		i = rand() % _N_;
		j = rand() % _M_;
		size = blocks[i][j]->getContentSize();
		while (size.width <= 0 && good)
		{
			i = rand() % _N_;
			j = rand() % _M_;
			size = blocks[i][j]->getContentSize();
		}
		
	}
	blockTime[i][j] = 0;
	//animation bullet and resize block
	animationBullet.pushBack(MoveTo::create(timeaction, Point(blocks[i][j]->getPositionX(), blocks[i][j]->getPositionY())));
	animationBullet.pushBack(RemoveSelf::create(true));
	animationBullet.pushBack(cocos2d::CallFunc::create([=]()->void 
		{
			//firts rebound
			if (blockType[i][j] == "coubrebound.png" && baf)
				shoot(blocks[i][j]->getPosition(), "rebound.png", false, 3, NULL);
			//second rebound
			if (countRebound>0)
			{
				shoot(blocks[i][j]->getPosition(),"rebound.png",false, countRebound-1,blocks[i][j]);
			}
			//fraction on 3 bullet
			if (baf==true && blockType[i][j] == "coubfraction.png")//and blocktype
			{
				for (int count=0;count<3;count++)
				shoot(blocks[i][j]->getPosition(), "fraction.png", false, 0, NULL);
			}

			//resize blocks and check until game is over
			if (currentScore < _N_ * _M_ * 150)
			{
				Size temp;
				temp = blocks[i][j]->getContentSize();
				if (temp.height <= 0) shoot(pos, image, baf, countRebound, prevBlock);
				else
				{
					//animation of type block
					if (blockType[i][j] == "coubrebound.png")
					{
						cocos2d::Vector<cocos2d::FiniteTimeAction*> animation;
						animation.pushBack(MoveBy::create(timeaction, Point(-3, 0)));
						animation.pushBack(MoveBy::create(timeaction, Point(3, 0)));
						auto sequence = cocos2d::Sequence::create(animation);
						blocks[i][j]->runAction(EaseIn::create(sequence, 0.5));
					}
					if (blockType[i][j] == "coubregeneration.png")
					{
						cocos2d::Vector<cocos2d::FiniteTimeAction*> animation;
						animation.pushBack(cocos2d::FadeTo::create(timeaction, 30));
						animation.pushBack(cocos2d::FadeTo::create(timeaction, 255));
						auto sequence = cocos2d::Sequence::create(animation);
						blocks[i][j]->runAction(EaseIn::create(sequence, 0.5));
					}
					if (blockType[i][j] == "coubfraction.png")
					{
						cocos2d::Vector<cocos2d::FiniteTimeAction*> animation;
						animation.pushBack(cocos2d::RotateBy::create(timeaction,15));
						animation.pushBack(cocos2d::RotateBy::create(timeaction,-30));
						animation.pushBack(cocos2d::RotateBy::create(timeaction, 15));
						auto sequence = cocos2d::Sequence::create(animation);
						blocks[i][j]->runAction(EaseIn::create(sequence, 0.5));
					}
					temp.height -= 6;
					temp.width -= 6;
					blocks[i][j]->setContentSize(temp);
					blocks[i][j]->setPosition(blocks[i][j]->getPositionX() + 3, blocks[i][j]->getPositionY() + 3);
					if (blocks[i][j]->getContentSize().width <= 0) currentScore += 100;
					currentScore += 10;
				}
			}
			score->setString("Score: " + std::to_string(currentScore));
			if (currentScore >= _N_ * _M_ * 150)//if game is over create Label and Button "Restart"
			{
				//print score and time (0.001f)
				std::string tg = std::to_string(timeGame);
				tg.resize(tg.size() - 3);
				score->setString("Score: " + std::to_string(currentScore)+"; Time: "+tg);

				this->unschedule(schedule_selector(Game::UpdateTimer));

				auto visibleSize = Director::getInstance()->getVisibleSize();
				auto backGround = Sprite::create("backGround.png");
				backGround->setContentSize(Size(visibleSize.width, visibleSize.height));
				backGround->setAnchorPoint(Vec2(0.0, 0.0));
				this->addChild(backGround);

				auto label = Label::createWithSystemFont("GAME OVER!", "Arial", 48);
				label->setPosition(Vec2(visibleSize.width / 2, visibleSize.height -200));
				this->addChild(label);

				auto restart = MenuItemImage::create(
					"restart.png",
					"restart2.png",
					CC_CALLBACK_1(Game::onClickRestart, this));
				restart->setPosition(Vec2(visibleSize.width / 2, visibleSize.height / 2 - 100));
				auto menu = Menu::create(restart, NULL);
				menu->setPosition(Vec2::ZERO);
				menu->setContentSize(Size(200, 80));
				this->addChild(menu, 1);
				
				score->updateOrderOfArrival();
				score->setPosition(Vec2(visibleSize.width / 2, 0 + 50));

				good = false;
			}

		}));
		
	auto sequence = cocos2d::Sequence::create(animationBullet);
	this->addChild(bullet);
	bullet->runAction(sequence);
}

bool Game::onTouchBegan(Touch* touch, Event* event)//Touch on screen
{
	labelTouch->setPosition(touch->getLocation());
	labelTouch->setVisible(true);
	if (good) shoot(touch->getLocation(),"bullet.png",true,0, NULL);
	return true;
}

void Game::onClickRestart(cocos2d::Ref* pSender)//click on button "Restart"
{
	this->removeAllChildren();
	reloaded();
}

void Game::onClickClose(cocos2d::Ref* pSender)//click on button "Restart"
{
	Director::getInstance()->end();
}

void Game::UpdateTimer(float dt)
{
	timeGame += dt;
	for (int i = 0; i < _N_; i++)
		for (int j = 0; j < _M_; j++)
		{
			if ( blockType[i][j] == "coubregeneration.png")
			{
				blockTime[i][j] += dt;
			}
			//if not shoot 3 second - regeneration
			if (blockTime[i][j] >= 3)
			{
				if (blocks[i][j]->getContentSize().height > 0 && blocks[i][j]->getContentSize().height < 30)
				{
					Size temp;
					temp = blocks[i][j]->getContentSize();
					temp.height += 6;
					temp.width += 6;
					blocks[i][j]->setContentSize(temp);
					blocks[i][j]->setPosition(blocks[i][j]->getPositionX() - 3, blocks[i][j]->getPositionY() - 3);
					currentScore -= 10;
					cocos2d::Vector<cocos2d::FiniteTimeAction*> animation;
					animation.pushBack(cocos2d::FadeTo::create(timeaction, 30));
					animation.pushBack(cocos2d::FadeTo::create(timeaction, 255));
					auto sequence = cocos2d::Sequence::create(animation);
					blocks[i][j]->runAction(EaseIn::create(sequence, 0.5));
				}
				blockTime[i][j] = 0;
				score->setString("Score: " + std::to_string(currentScore));
			}
		}
}